# Temporal Entailment Sports Dataset

This repository contains the evaluation dataset used in the following paper:

Incorporating Temporal Information in Entailment Graph Mining  
Liane Guillou*, Sander Bijl De Vroe*, Mohammad Javad Hosseini, Mark Johnson and Mark Steedman. TextGraphs 2020, 13 December 2020.

The dataset consists of entailment pairs for the sports domain. The file sports_entailment_pairs.txt is a tab separated file, where each line contains an entailment relation between two predicates P and Q. Column 1 contains predicate P, column 2 contains predicate Q, and column 3 contains entailment type.

There are four possible values for _entailment type_:

- **0** = non-entailment; P does not entail Q
- **1** = entailment; P entails Q
- **-1** = directional non-entailment, i.e. the set of entailments with direction reversed; P does not entail Q
- **-2** = paraphrase; P is a paraphrase of Q

It is possible to transform this into a regular entailment dataset by replacing -1 with 0 (non-entailment) and -2 with 1 (entailment).

\* Please direct any queries regarding the dataset to the first authors:  
<liane.guillou@ed.ac.uk>  
<sbdv@ed.ac.uk>